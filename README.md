# 2023_long_2d_precision

Y. Long, R. Vetter, D. Iber, *2D Effects Enhance Precision of Gradient-Based Tissue Patterning*

Preprint available on BioRxiv at https://doi.org/10.1101/2023.03.13.532369


## Contents

* fig2.m - MATLAB script generating the data for Figure 2
* fig3bc.m - MATLAB script generating the data for Figure 3 B,C
* fig3de.m - MATLAB script generating the data for Figure 3 D,E
* fig4b.m - MATLAB script generating the data for Figure 4 B
* fig4cd.m - MATLAB script generating the data for Figure 4 C,D
* figS4.m - MATLAB script generating the data for Figure S4
* fdm_solver.m - Finite difference solver in MATLAB, used by figX.m
* LICENSE - License file


## Installation and system requirements

The MATLAB scripts were produced with version R2021b and require the Statistics and Machine Learning Toolbox (stats).
No further installations are required, and no further system limitations apply.


## Usage instructions

The MATLAB scripts "figX.m" can be executed out-of-the-box by just running them in MATLAB.


## Input/Output

No manual specification of input or output is required.
The MATLAB scripts produce as output the data plotted in the paper, in the form of comma-separated value files.
Local data folders are generated automatically for each script and the data is written into these folders. 


## License

All source code is released under the 3-Clause BSD license (see LICENSE for details).
