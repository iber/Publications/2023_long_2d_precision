% Copyright © 2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Yuchong Long, Roman Vetter & Dagmar Iber

function C_new = fdm_solver(C_old, p, d, D, a, hx, hy, tol, zeroflux)
            
    % C_old: initial concentration
    % p: production rates
    % d: degradation rates
    % D: diffusion rates
    % a: degree of orthotropy (Dy/Dx)
    % hx: grid spacing in x direction
    % hy: grid spacing in y direction
    % tol: error tolerance
    % periodic: boolean flag indicating the boundary conditions in y direction
    
    err = inf;
    Cx = NaN(size(C_old));
    Cy = Cx;
    while err > tol
        % zero-flux boundary conditions in x direction
        Cx(1,:) = 2 * C_old(2,:);
        Cx(end,:) = 2 * C_old(end-1,:);
        
        if zeroflux
            % zero-flux boundary conditions in y direction
            Cy(:,1) = 2 * C_old(:,2);
            Cy(:,end) = 2 * C_old(:,end-1);
        else
            % periodic boundary conditions in y direction
            Cy(:,1) = C_old(:,2) + C_old(:,end);
            Cy(:,end) = C_old(:,end-1) + C_old(:,1);
        end

        % 5-point central difference stencil
        Cx(2:end-1,:) = C_old(1:end-2,:) + C_old(3:end,:);
        Cy(:,2:end-1) = C_old(:,1:end-2) + C_old(:,3:end);
        C_new = (p + D .* (Cx/hx^2 + a*Cy/hy^2)) ./ (d + 2*D*(1/hx^2 + a/hy^2));
        err = max(abs(C_new - C_old), [], 'all');
        C_old = C_new;
    end

end
