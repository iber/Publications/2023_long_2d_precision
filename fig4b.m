% Copyright © 2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Yuchong Long, Roman Vetter & Dagmar Iber

function fig4b

% Simulate patterning precision in the growing Drosophila wing disc, in which the
% gradient length and source size increase in parallel to the wing pouch length,
% while the cells get narrower at the same time.

% options
simulate = true; % if false, plot results from saved files instead of generating new data
writeresults = true; % when creating new data, should the results be written to output files?
fitcosh = true; % fit an exp or cosh to the gradients
checkbounds = true; % make sure fitted lambda and C0 are positive
plotresults = true; % plot resulting data or not
LineWidth = 1;
FontSize = 18;

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1e3; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
res = 3; % linear grid resolution of each cell
mu_D = 0.033; % mean morphogen diffusion constant [µm^2/s]
CV = 0.3; % coefficient of variation of the kinetic parameters
Ny = 10; % number of cells in transverse direction
Lp = 50:25:300; % patterning domain lengths [µm]
Ls = 0.16 * Lp; % morphogen source lengths [µm] (fit to data from Wartlick et al. 2011)
mu_lambda = 0.11 * Lp; % mean gradient length [µm] from Wartlick et al. 2011
diameter = linspace(4.5,1.5,numel(Lp)); % cell diameter [µm] (decreases over time)

% create folder to save files in if it does not already exist
dir = 'fig4b';
if not(isfolder(dir))
    mkdir(dir)
end

% log-normal distribution with adjusted mean & CV
logndist = @(mu, CV) makedist('Lognormal', 'mu', log(mu/sqrt(1+CV^2)), 'sigma', sqrt(log(1+CV^2)));

fitopt = statset('TolFun', tol, 'TolX', tol);


if simulate
    sigma_x = NaN(numel(Lp),1);
    sigma_x_SE = sigma_x;
    
    % loop over domain lengths
    tic
    for i = 1:numel(Lp)
        Lp(i) % print progress
        
        % derived variables
        mu_d = mu_D/mu_lambda(i)^2; % mean morphogen degradation rate [1/s]
        mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)] 
        Np = round(Lp(i)/diameter(i)); % number of cells along the patterning domain
        Ns = round(Ls(i)/diameter(i)); % number of cells along the source domain
        Nx = Ns + Np; % total number of cells along the patterning axis
        h = diameter(i) / res; % grid spacing

        mu_x = 0.4 * Lp(i); % readout position at 40% of Lp 

        % analytical deterministic solution
        C = @(x) mu_p/mu_d * ((x<0) .* (1-cosh(x/mu_lambda(i))) + sinh(Ls(i)/mu_lambda(i)) / sinh((Ls(i)+Lp(i))/mu_lambda(i)) * cosh((Lp(i)-x)/mu_lambda(i)));

        % loop over several independent runs
        lambda = NaN(nruns, Ny);
        C0 = lambda;
        x_theta = NaN(nruns, Ny);
        for k = 1:nruns

            % draw random kinetic parameters for each cell
            p = random(logndist(mu_p, CV), Nx, Ny);
            d = random(logndist(mu_d, CV), Nx, Ny);
            D = random(logndist(mu_D, CV), Nx, Ny);
            p(Ns+1:end,:) = 0; % no production in the patterning domain
    
            % inflate them to the grid
            p = repelem(p, res, res);
            d = repelem(d, res, res);
            D = repelem(D, res, res);
    
            % deterministic solution as initial guess
            x = linspace(-Ls(i), Lp(i)-h, Nx * res)' + h/2;
            C_old = C(x) * ones(1, Ny * res);

            % solve the reaction-diffusion equation
            C_new = fdm_solver(C_old, p, d, D, 1, h, h, tol, true);

            % for each cell in y direction
            for y = 1:Ny
                % determine the cellular readout concentrations
                yidx = (y-1)*res+1:y*res;
                C_readout = mean(C_new(:,yidx), 2);
                
                % fit an exponential in log space in the patterning domain
                x = linspace(-Ls(i), Lp(i)-h, Nx * res)' + h/2;
                param = polyfit(x, log(C_readout), 1);
                lambda(k,y) = -1/param(1);
                C0(k,y) = exp(param(2));
        
                % fit a hyperbolic cosine in log space in the patterning domain
                if fitcosh
                    logcosh = @(p,x) p(2) + log(cosh((Lp(i)-x)/p(1)));
                    mdl = fitnlm(x, log(C_readout), logcosh, [lambda(k,y) log(C0(k,y)) - log(cosh(Lp(i)/lambda(k,y)))], 'Options', fitopt);
                    lambda(k,y) = mdl.Coefficients.Estimate(1);
                    C0(k,y) = exp(mdl.Coefficients.Estimate(2)) * cosh(Lp(i)/lambda(k,y));
                end

                % discard out-of-bound values
                if checkbounds
                    if lambda(k,y) <= 0 || C0(k,y) <= 0
                        lambda(k,y) = NaN;
                        C0(k,y) = NaN;
                    end
                end
            
                % threshold concentration
                C_theta = C(mu_x);

                % determine the readout position
                indices = find(diff(sign(C_readout - C_theta)));
                if isempty(indices)
                    continue
                end
                
                x_theta_all = [];
                for idx = indices
                    x_theta_all = [x_theta_all, interp1(C_readout([idx idx+1]), x([idx idx+1]), C_theta)];
                end
                x_theta(k,y) = mean(x_theta_all);

            end
        end

        % determine the average positional error across all cell rows, over the independent runs
        % and also their standard errors from bootstrapping
        sigma_x(i) = nanstd(nanmean(x_theta,2));
        sigma_x_SE(i) = nanstd(bootstrp(nboot, @(x) nanstd(nanmean(x,2)), x_theta));
    end
    toc
    
    % write results to output files
    if writeresults
        T = table();
        T.Lp = Lp';
        T.sigma_x = sigma_x;
        T.sigma_x_SE = sigma_x_SE;
        writetable(T, [dir '/wing_disc_positional_error.csv']);
    end
else
    % read existing data
    T = readtable([dir '/wing_disc_positional_error.csv']);
    Lp = T.Lp';
    sigma_x = T.sigma_x;
    sigma_x_SE = T.sigma_x_SE;
end

% plot results
if plotresults
    close all
    figure
    errorbar(Lp', sigma_x, sigma_x_SE, 'LineWidth', LineWidth)
    xlabel('Patterning domain length L_p [\mum]')
    ylabel('Positional error \sigma_x [\mum]')
    set(gca, 'LineWidth', LineWidth, 'FontSize', FontSize)
    xlim([0 300])
end

end