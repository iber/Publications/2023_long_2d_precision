% Copyright © 2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Yuchong Long, Roman Vetter & Dagmar Iber

function fig4cd

% Solve the reaction-diffusion equation with fixed patterning length and 
% identical cell diameters in x and y directions, calculate the gradient 
% positional error as a function of the cell diameter.

% options
simulate = true; % if false, plot results from saved files instead of generating new data
writeresults = true; % when creating new data, should the results be written to output files?
plotresults = true; % plot resulting data or not
zeroflux = true; % use zero-flux boundary conditions, otherwise periodic boundary conditions
LineWidth = 1;
FontSize = 18;

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1e3; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
res = 3; % linear grid resolution of each cell
diameter = 1:40; % cell diameters to loop over [µm]
mu_D = 0.033; % mean morphogen diffusion constant [µm^2/s]
mu_lambda = 20; % mean gradient length [µm]
mu_d = mu_D/mu_lambda^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]
CV = 0.3; % coefficient of variation of the kinetic parameters
Ls = 25; % source length [µm]
Lp = 250; % pattern length [µm]
Ny = [1 4 10 40]; % number of cells in transverse direction
readout = 9; % readout position in units of mu_lambda
colors = [0, 0.4470, 0.7410;...
          0.8500, 0.3250, 0.0980;...
          0.9290, 0.6940, 0.1250;...
          0, 0.5, 0]; % plot colors for the different domain widths

% create folder to save files in if it does not already exist
dir = 'fig4cd';
if not(isfolder(dir))
    mkdir(dir)
end

% log-normal distribution with adjusted mean & CV
logndist = @(mu, CV) makedist('Lognormal', 'mu', log(mu/sqrt(1+CV^2)), 'sigma', sqrt(log(1+CV^2)));


if simulate
    sigma_x = NaN(numel(diameter), numel(Ny));
    sigma_x_SE = sigma_x;
    
    % loop over cell diameters
    tic
    for i = 1:numel(diameter)
        diameter(i) % print progress

        % If the last cell exceeds the source domain boundary, determine
        % whether or not the source length is closer to the target
        % without it. Remove the last cell if the source length is 
        % closer to the target without it, keep it otherwise.
        if mod(Ls/diameter(i),1) == 0
            Ns = Ls/diameter(i);
        elseif mod(Ls/diameter(i),1) < 0.5
            Ns = floor(Ls/diameter(i));
            Ls = Ns * diameter(i);
        else
            Ns = ceil(Ls/diameter(i));
            Ls = Ns * diameter(i);
        end

        % same for the patterning domain length
        if mod(Lp/diameter(i),1) == 0
            Np = Lp/diameter(i);
        elseif mod(Lp/diameter(i),1) < 0.5
            Np = floor(Lp/diameter(i));
            Lp = Np * diameter(i);
        else
            Np = ceil(Lp/diameter(i));
            Lp = Np * diameter(i);
        end

        Nx = Ns + Np; % total number of cells along the patterning axis
        h = diameter(i) / res; % grid spacing
        
        % analytical deterministic solution
        C = @(x) mu_p/mu_d * ((x<0) .* (1-cosh(x/mu_lambda)) + sinh(Ls/mu_lambda) / sinh((Ls+Lp)/mu_lambda) * cosh((Lp-x)/mu_lambda));

        % loop over tissue widths
        for j = 1:numel(Ny)

            x_theta = NaN(nruns, 1);
        
            % loop over several independent runs
            for k = 1:nruns

                % draw random kinetic parameters for each cell
                p = random(logndist(mu_p, CV), Nx, Ny(j));
                d = random(logndist(mu_d, CV), Nx, Ny(j));
                D = random(logndist(mu_D, CV), Nx, Ny(j));
                p(Ns+1:end,:) = 0; % no production in the patterning domain
        
                % inflate them to the grid
                p = repelem(p, res, res);
                d = repelem(d, res, res);
                D = repelem(D, res, res);
        
                % deterministic solution as initial guess
                x = linspace(-Ls, Lp-h, Nx * res)' + h/2;
                C_old = C(x) * ones(1, Ny(j) * res);
                
                % solve the reaction-diffusion equation
                C_new = fdm_solver(C_old, p, d, D, 1, h, h, tol, zeroflux);
    
                % determine the cellular readout concentrations
                y = ceil(Ny(j)/2); % a single cell row at the middle of the domain
                yidx = (y-1)*res+1:y*res;
                C_readout = mean(C_new(:,yidx), 2);
            
                % threshold concentration
                C_theta = C(readout * mu_lambda);

                % determine the readout position
                indices = find(diff(sign(C_readout - C_theta)));
                
                x_theta_all = [];
                if ~isempty(indices)
                    for idx = indices
                        x_theta_all = [x_theta_all, interp1(C_readout([idx idx+1]), x([idx idx+1]), C_theta)];
                    end
                end
                x_theta(k) = mean(x_theta_all);
            end
    
            % determine the positional error over the independent runs
            % and also its standard error from bootstrapping
            sigma_x(i,j) = nanstd(x_theta);            
            sigma_x_SE(i,j) = nanstd(bootstrp(nboot, @nanstd, x_theta));
        end
    end
    toc
    
    % write results to output files
    if writeresults
        for j = 1:numel(Ny)
            T = table();
            T.diameter = diameter';
            T.sigma_x = sigma_x(:,j);
            T.sigma_x_SE = sigma_x_SE(:,j);
            if zeroflux
                writetable(T, [dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_zeroflux.csv']);
            else
                writetable(T, [dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_periodic.csv']);
            end
        end
    end
else
    % read existing data
    for j = 1:numel(Ny)
        if zeroflux
            T = readtable([dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_zeroflux.csv']);
        else
            T = readtable([dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_periodic.csv']);
        end
        diameter = T.diameter';
        sigma_x(:,j) = T.sigma_x;
        sigma_x_SE(:,j) = T.sigma_x_SE;
    end
end

% plot results
if plotresults
    close all
    figure('Position', [0 0 1100 500]);
    
    for j = 1:numel(Ny)
        name = ['Ny = ' num2str(Ny(j))];

        subplot(1,2,1)
        hold on
        errorbar(diameter, sigma_x(:,j)', sigma_x_SE(:,j)', 'o', 'DisplayName', name, 'LineWidth', LineWidth, 'Color', colors(j,:))
        xlabel('Cell diameter  \delta  (µm)')
        ylabel('Absolute positional error  \sigma_x  (µm)')
        legend('show')
        set(gca, 'LineWidth', LineWidth, 'FontSize', FontSize)
        box on    
        xlim([0 max(diameter)])
        ylim([0 25])

        subplot(1,2,2)
        hold on
        errorbar(diameter, sigma_x(:,j)' ./ diameter, sigma_x_SE(:,j)' ./ diameter, 'o', 'DisplayName', name, 'LineWidth', LineWidth, 'Color', colors(j,:))
        xlabel('Cell diameter  \delta  (µm)')
        ylabel('Relative positional error  \sigma_x / \delta  (cells)')
        legend('show')
        set(gca, 'LineWidth', LineWidth, 'FontSize', FontSize)
        box on    
        xlim([0 max(diameter)])
        ylim([0 4])
    end
end

end