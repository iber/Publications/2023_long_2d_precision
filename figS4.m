% Copyright © 2023
% ETH Zurich
% Department of Biosystems Science and Engineering
% Yuchong Long, Roman Vetter & Dagmar Iber

function figS4

% Solve the reaction-diffusion equation with fixed diffusivity in x
% direction, calculate the gradient variability and positional error
% as a function of the degree of othotropy in diffusion.

% options
simulate = true; % if false, plot results from saved files instead of generating new data
writeresults = true; % when creating new data, should the results be written to output files?
fitcosh = true; % fit an exp or cosh to the gradients
checkbounds = true; % make sure fitted lambda and C0 are positive
plotresults = true; % plot resulting data or not
zeroflux = true; % use zero-flux boundary conditions, otherwise periodic boundary conditions
LineWidth = 1;
FontSize = 18;

% parameters
tol = 1e-10; % numerical tolerance for solver and fitting
nruns = 1e3; % number of independent simulation runs
nboot = 1e4; % number of bootstrap samples for error estimation
res = 3; % linear grid resolution of each cell
diameter = 5; % cell diameter [µm]
mu_D = 0.033; % mean morphogen diffusion constant [µm^2/s]
mu_lambda = 20; % mean gradient length [µm]
mu_d = mu_D/mu_lambda^2; % mean morphogen degradation rate [1/s]
mu_p = mu_d; % mean morphogen production rate [substance/(µm^3*s)]
CV = 0.3; % coefficient of variation of the kinetic parameters
Ns = 5; % number of cells along the source domain
Np = 50; % number of cells along the patterning domain
Ny = [1 10 40]; % number of cells in transverse direction
alpha = logspace(-1, 1, 11); % degrees of diffusion orthotropy
readout = 6; % readout position in units of mu_lambda
colors = [0, 0.4470, 0.7410;...
          0.8500, 0.3250, 0.0980;...
          0.9290, 0.6940, 0.1250]; % plot colors for the different tissue widths

% derived variables
Nx = Ns + Np; % total number of cells along the patterning axis
Ls = Ns * diameter; % source length
Lp = Np * diameter; % pattern length
h = diameter / res; % grid spacing

% create folder to save files in if it does not already exist
dir = 'figS4';
if not(isfolder(dir))
    mkdir(dir)
end

% analytical deterministic solution
C = @(x) mu_p/mu_d * ((x<0) .* (1-cosh(x/mu_lambda)) + sinh(Ls/mu_lambda) / sinh((Ls+Lp)/mu_lambda) * cosh((Lp-x)/mu_lambda));

% log-normal distribution with adjusted mean & CV
logndist = @(mu, CV) makedist('Lognormal', 'mu', log(mu/sqrt(1+CV^2)), 'sigma', sqrt(log(1+CV^2)));

CVfun = @(x) nanstd(x) / nanmean(x);

fitopt = statset('TolFun', tol, 'TolX', tol);


if simulate
    CV_lambda = NaN(numel(alpha), numel(Ny));
    CV_lambda_SE = CV_lambda;
    CV_0 = CV_lambda;
    CV_0_SE = CV_lambda;
    sigma_x = CV_lambda;
    sigma_x_SE = CV_lambda;
    
    % loop over degrees of diffusion anisotropy
    tic
    for i = 1:numel(alpha)
        alpha(i) % print progress

        lambda = NaN(nruns, numel(Ny));
        C0 = lambda;
        x_theta = NaN(nruns, numel(Ny));

        % loop over tissue widths
        for j = 1:numel(Ny)
            Ny(j) % print progress
        
            % loop over several independent runs
            for k = 1:nruns
    
                % draw random kinetic parameters for each cell
                p = random(logndist(mu_p, CV), Nx, Ny(j));
                d = random(logndist(mu_d, CV), Nx, Ny(j));
                D = random(logndist(mu_D, CV), Nx, Ny(j));
                p(Ns+1:end,:) = 0; % no production in the patterning domain
        
                % inflate them to the grid
                p = repelem(p, res, res);
                d = repelem(d, res, res);
                D = repelem(D, res, res);
        
                % deterministic solution as initial guess
                x = linspace(-Ls, Lp-h, Nx * res)' + h/2;
                C_old = C(x) * ones(1, Ny(j) * res);
                
                % solve the reaction-diffusion equation
                C_new = fdm_solver(C_old, p, d, D, alpha(i), h, h, tol, zeroflux);
    
                % determine the cellular readout concentrations
                y = ceil(Ny/2); % a single cell row at the middle of the domain
                yidx = (y-1)*res+1:y*res;
                C_readout = mean(C_new(:,yidx), 2);
                
                % fit an exponential in log space in the patterning domain
                param = polyfit(x, log(C_readout), 1);
                lambda(k,j) = -1/param(1);
                C0(k,j) = exp(param(2));
        
                % fit a hyperbolic cosine in log space in the patterning domain
                if fitcosh
                    logcosh = @(p,x) p(2) + log(cosh((Lp-x)/p(1)));
                    mdl = fitnlm(x, log(C_readout), logcosh, [lambda(k,j) log(C0(k,j)) - log(cosh(Lp/lambda(k,j)))], 'Options', fitopt);
                    lambda(k,j) = mdl.Coefficients.Estimate(1);
                    C0(k,j) = exp(mdl.Coefficients.Estimate(2)) * cosh(Lp/lambda(k,j));
                end

                % discard out-of-bound values
                if checkbounds
                    if lambda(k,j) <= 0 || C0(k,j) <= 0
                        lambda(k,j) = NaN;
                        C0(k,j) = NaN;
                    end
                end
            
                % threshold concentration
                C_theta = C(readout * mu_lambda);

                % determine the readout position
                indices = find(diff(sign(C_readout - C_theta)));
                
                x_theta_all = [];
                for idx = indices
                    x_theta_all = [x_theta_all, interp1(C_readout([idx idx+1]), x([idx idx+1]), C_theta)];
                end
                x_theta(k,j) = mean(x_theta_all);
            end
    
            % determine the CV of the decay length and the amplitude, and the positional error, over the independent runs
            % and also their standard errors from bootstrapping
            CV_lambda(i,j) = CVfun(lambda(:,j));
            CV_lambda_SE(i,j) = nanstd(bootstrp(nboot, CVfun, lambda(:,j)));
            CV_0(i,j) = CVfun(C0(:,j));
            CV_0_SE(i,j) = nanstd(bootstrp(nboot, CVfun, C0(:,j)));
            sigma_x(i,j) = nanstd(x_theta(:,j));            
            sigma_x_SE(i,j) = nanstd(bootstrp(nboot, @nanstd, x_theta(:,j)));
        end
    end
    toc
    
    % write results to output files
    if writeresults
        for j = 1:numel(Ny)
            T = table();
            T.alpha = alpha';
            T.CV_lambda = CV_lambda(:,j);
            T.CV_lambda_SE = CV_lambda_SE(:,j);
            T.CV_0 = CV_0(:,j);
            T.CV_0_SE = CV_0_SE(:,j);
            T.sigma_x = sigma_x(:,j);
            T.sigma_x_SE = sigma_x_SE(:,j);
            if zeroflux
                writetable(T, [dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_zeroflux.csv']);
            else
                writetable(T, [dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_periodic.csv']);
            end
        end
    end
else
    % read existing data
    for j = 1:numel(Ny)
        if zeroflux
            T = readtable([dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_zeroflux.csv']);
        else
            T = readtable([dir '/readout_' num2str(readout) 'lambda_' num2str(Ny(j)) 'cells_periodic.csv']);
        end
        alpha = T.alpha';
        CV_lambda(:,j) = T.CV_lambda;
        CV_lambda_SE(:,j) = T.CV_lambda_SE;
        CV_0(:,j) = T.CV_0;
        CV_0_SE(:,j) = T.CV_0_SE;
        sigma_x(:,j) = T.sigma_x;
        sigma_x_SE(:,j) = T.sigma_x_SE;
    end
end

% plot results
if plotresults
    close all
    figure('Position', [0 0 1600 500]);
    
    for j = 1:numel(Ny)
        name = ['Ny = ' num2str(Ny(j))];
        
        subplot(1,3,1)
        hold on
        if Ny(j) > 1
            mdl = fitnlm(alpha, CV_lambda(:,j), @(p,x) p(1)./sqrt(x)+p(2), [1 0]);
            plot(alpha, feval(mdl, alpha), '-', 'HandleVisibility', 'off', 'LineWidth', LineWidth, 'Color', colors(j,:))
        end
        errorbar(alpha, CV_lambda(:,j), CV_lambda_SE(:,j), 'o', 'LineWidth', LineWidth, 'Color', colors(j,:))
        xlabel('Degree of diffusion orthotropy  \alpha')
        ylabel('Decay length variability  CV_\lambda')
        set(gca, 'LineWidth', LineWidth, 'FontSize', FontSize)
        box on
        xlim([min(alpha) max(alpha)])
    
        subplot(1,3,2)
        hold on
        if Ny(j) > 1
            mdl = fitnlm(alpha, CV_0(:,j), @(p,x) p(1)./sqrt(x)+p(2), [1 0]);
            plot(alpha, feval(mdl, alpha), '-', 'HandleVisibility', 'off', 'LineWidth', LineWidth, 'Color', colors(j,:))
        end
        errorbar(alpha, CV_0(:,j), CV_0_SE(:,j), 'o', 'LineWidth', LineWidth, 'Color', colors(j,:))
        xlabel('Degree of diffusion orthotropy  \alpha')
        ylabel('Amplitude variability  CV_0')
        set(gca, 'LineWidth', LineWidth, 'FontSize', FontSize)
        box on
        xlim([min(alpha) max(alpha)])
    
        subplot(1,3,3)
        hold on
        if Ny(j) > 1
            mdl = fitnlm(alpha, sigma_x(:,j), @(p,x) p(1)./sqrt(x)+p(2), [1 0]);
            plot(alpha, feval(mdl, alpha) / diameter, '-', 'HandleVisibility', 'off', 'LineWidth', LineWidth, 'Color', colors(j,:))
        end
        errorbar(alpha, sigma_x(:,j) / diameter, sigma_x_SE(:,j) / diameter, 'o', 'DisplayName', name, 'LineWidth', LineWidth, 'Color', colors(j,:))
        xlabel('Degree of diffusion orthotropy  \alpha')
        ylabel('Relative positional error  \sigma_x / \delta_x (cells)')
        legend('show')
        set(gca, 'LineWidth', LineWidth, 'FontSize', FontSize)
        box on    
        xlim([min(alpha) max(alpha)])
    end
end

end